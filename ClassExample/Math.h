#ifndef MATH_H
#define MATH_H

class Math {

private:

	int num1;
	int num2;

public:
	void setNum(int number1, int number2);
	
	int getNum1(void);

	int getNum2(void);

	int addTwoNum(int number1, int number2);

	// Sub two numbers
	int subTwoNum(int number1, int number2);

	// Multi two numbers
	int mulTwoNum(void);

	// Div two numbers
	int divTwoNum(int number1, int number2);

	// Factorial
	int factNum(void);
	//int factNum(int number1);

};


#endif
