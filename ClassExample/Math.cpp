#include <iostream>
#include <iomanip>
#include "math.h"
using namespace std;


void Math::setNum(int number1, int number2) {
	num1 = number1;
	num2 = number2;
}

int Math::getNum1(void) {

	return num1;
}

int Math::getNum2(void) {

	return num2;
}


int Math::addTwoNum(int number1, int number2)
{
	return (number1 + number2);
}

int Math::subTwoNum(int number1, int number2)
{
	return (number1 - number2);
}

//int Math::mulTwoNum(int number1, int number2)
int Math::mulTwoNum(void)
{
	int number1 = num1;
	int number2 = num2;
	return (number1 * number2);
}

int Math::divTwoNum(int number1, int number2)
{
	return (number1 / number2);
}

//int Math::factNum(int number1)
int Math::factNum(void)
{
	int number1 = num1;
	static int factorial = 1;
	static int i;

	for (int i = 1; i <= number1; ++i)
	{
		factorial = (factorial * i);
	}
	return (factorial);
}
